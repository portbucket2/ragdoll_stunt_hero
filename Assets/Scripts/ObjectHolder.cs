using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaiyaJinn
{
    public class ObjectHolder : MonoBehaviour
    {
        public GameObject gameObject;

        public GameObject GameObject { get => gameObject; }

    }
}
