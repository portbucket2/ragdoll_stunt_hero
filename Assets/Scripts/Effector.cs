using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SaiyaJinn
{
    public class Effector : MonoBehaviour
    {
        public Transform effectorTranform;
        public Image sprite;
        private Camera mainCamera;
        private LayerMask layer = 1 << 8;
        [SerializeField]
        private Transform m_initalPosition;
        [SerializeField]
        private float clampDistance;
        private Vector3 m_newPosition;
        private Color color;
        


        private void Start()
        {
            mainCamera = FindObjectOfType<Camera>();
            effectorTranform = GetComponent<Transform>();
            color = sprite.color;
            //m_initalPosition = effectorTranform.position;
        }
        private void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                
            }
            else if(Input.GetMouseButton(0))
            {
                Move();
            }
            else if(Input.GetMouseButtonUp(0))
            {
                sprite.color = color;
            }
        }
        public  void Move()
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if(Physics.Raycast(ray, out hitInfo, layer))
            {
                if(hitInfo.collider.CompareTag(effectorTranform.tag))
                {
                    m_newPosition = new Vector3(hitInfo.point.x, hitInfo.point.y, effectorTranform.position.z);
                    sprite.color = Color.green;
                    Vector3 offset = m_newPosition - m_initalPosition.position;
                    transform.position = m_initalPosition.position +  Vector3.ClampMagnitude(offset, clampDistance);
                }
                    

            }
        }

        public GameObject EffectorSelection()
        {
            GameObject gameObject = null;
            return gameObject;
        }
    }
}
