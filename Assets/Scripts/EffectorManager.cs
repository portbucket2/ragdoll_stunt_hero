using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using UnityEngine.UI;
using UnityEngine.Events;

namespace SaiyaJinn
{
    public class EffectorManager : BaseEffector
    {
        private Camera mainCamera;
        public LayerMask layer = 1 << 8;
        private GameObject targetEffector;
        private bool isMouseDragging;
        private Vector3 screenPosition;
        private Vector3 offset;
        public float clampDistance = 1;
        private int pressCounter = 0;
        //public DragReporter dragHandler;
        public List<Transform> images = new List<Transform>();
        public UnityEvent ShowUpButton;

        void Start()
        {
            mainCamera = FindObjectOfType<Camera>();
            
            /*
            System.Action<Drag> p = delegate (Drag drag)
                           {
                               
                               if(!drag.didntDragFarEnough && drag != null)
                               {
                                   
                                   Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z);

                                   Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

                                   //It will update target gameobject's current postion.
                                   targetEffector.transform.position = currentPosition;
                               }
                           };
            dragHandler.onDrag += p;
            */

        }

        public override GameObject EffectorSelection(out RaycastHit hit)
        {
            GameObject targetObject = null;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction * 20, out hit, Mathf.Infinity, layer))
            {
                targetObject = hit.collider.gameObject.GetComponent<ObjectHolder>().GameObject;

            }
            
            return targetObject;
        }

        public override void Move()
        {
            
        }


        // Update is called once per frame
        void Update()
        {
            
            if(Input.GetMouseButtonDown(0))
            {
                RaycastHit hitInfo;
                targetEffector = EffectorSelection(out hitInfo);
                if(targetEffector != null)
                {
                    isMouseDragging = true;
                    pressCounter++;
                    if(pressCounter >=2 && ShowUpButton!=null)
                    {
                        ShowUpButton.Invoke();
                        
                    }
                    screenPosition = Camera.main.WorldToScreenPoint(targetEffector.transform.position);
                    offset = targetEffector.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z));
                }
            }
            
            else if(Input.GetMouseButtonUp(0))
            {
                isMouseDragging = false;
            }
            
            if(isMouseDragging)
            {
                Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z);
                
                Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + Vector3.ClampMagnitude(offset, clampDistance);

                //It will update target gameobject's current postion.
               targetEffector.transform.position = currentPosition;
                foreach(Transform i in images)
                {
                    
                    i.LookAt(mainCamera.transform.position, Vector3.up);
                }
            }
            
        }
        
    }
}
