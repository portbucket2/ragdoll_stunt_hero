using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SaiyaJinn
{
    [System.Serializable]
    public class CustomGameObjectEvent : UnityEvent<GameObject> { }
    public class GenericTrigger : MonoBehaviour
    {
        [Space(20.0f)]
        public string comparisonTag;
        [Space(20.0f)]
        public CustomGameObjectEvent onTriggerEnterCallback;
        public CustomGameObjectEvent onTriggerExitCallback;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(comparisonTag))
            {
                //Debug.Log(other.gameObject.name + "_TRIGGER_ENTERED_");

                onTriggerEnterCallback?.Invoke(other.gameObject);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(comparisonTag))
            {
                //Debug.Log(other.gameObject.name + "_TRIGGER_EXITED_");
                onTriggerExitCallback?.Invoke(other.gameObject);
            }
        }
    }
}
