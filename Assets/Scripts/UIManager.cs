using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;

namespace SaiyaJinn
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;
        public RectTransform startRectTransform;
        public RectTransform startTextRectTransform;

        Tween startGUITween;

        private void Awake()
        {
            Singleton();
        }
        private void Singleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

        }
        // Start is called before the first frame update
        void Start()
        {
            startGUITween = startTextRectTransform.DOScale(new Vector2(1.2f, 1.2f), .5f).SetLoops(-1, LoopType.Yoyo);
        }

        public void DisableStartGUI()
        {
            startGUITween.Kill();
            startRectTransform.DOScale(0, .15f).SetEase(Ease.InBack);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
