using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using DG.Tweening;
using PathCreation;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using System;
using Cinemachine;
using UnityEngine.SceneManagement;

namespace SaiyaJinn
{
    public class LevelOneTest : MonoBehaviour
    {
        [Title("Common Stuffs")]
        private Animator animator;
        public PathCreator pathCreator;
        public Button restartButton;
        public Button continueButton;
        private float distanceTravelled;
        public float speed;
        public CinemachineVirtualCamera vCam1;
        [Title("Booleans")]
        private bool isTapped = false;
        private bool isRunning = false;
        private bool isRagdoll = false;
        private bool isJumpFinished = false;

        //Hash
        int idle = Animator.StringToHash("idle");
        int running = Animator.StringToHash("Run");
        int jumping = Animator.StringToHash("Jump");
        int jumpDown = Animator.StringToHash("JumpDown");
        int celebrate = Animator.StringToHash("Celebrate");

        [Title("Bones & IK")]
        [SerializeField]
        private GameObject leftLegObject;
        [SerializeField]
        private GameObject rightLegObject;
        private TwoBoneIKConstraint leftLegTBIK;
        private TwoBoneIKConstraint rightLegTBIK;
        [SerializeField]
        private Transform leftFoot;
        [SerializeField]
        private Transform rightFoot;

        [Title("First Sequence")]
        public UnityEvent OnFirstSequence;
        [Title("Second Sequence")]
        public GameObject[] hitBoxs = new GameObject[2];
        public RectTransform uiButton;
        [Title("Third Sequence")]
        public List<Collider> colliders = new List<Collider>();
        public GameObject obstacle;

        private void Start()
        {
            StartCoroutine(CheckTapRoutine());
            animator = GetComponent<Animator>();
            leftLegTBIK = leftLegObject.GetComponent<TwoBoneIKConstraint>();
            rightLegTBIK = rightLegObject.GetComponent<TwoBoneIKConstraint>();
            if (pathCreator != null)
            {           
                pathCreator.pathUpdated += OnPathChanged;
            }
            continueButton.onClick.AddListener(ThirdSequence);
            restartButton.onClick.AddListener(RestartScene);
        }

        IEnumerator CheckTapRoutine()
        {
            yield return new WaitUntil(() => isTapped);
            if (OnFirstSequence != null)
                OnFirstSequence.Invoke();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) && !isTapped)
            {
                isTapped = true;
                UIManager.Instance.DisableStartGUI();
            }

            if(isRunning && pathCreator!= null)
            {
                distanceTravelled += speed * Time.deltaTime;
                transform.parent.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
            }
            if(isJumpFinished)
            {
                distanceTravelled += speed * Time.deltaTime;
                transform.parent.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
            }
        }
        void RestartScene()
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
            DOTween.KillAll();
        }
        public void FirstSequence()
        {
            animator.SetBool(idle, false);
            animator.SetBool(running, true);
            isRunning = true;
            
        }

        public void SecondSequence()
        {
            Debug.Log("Second Sequence");
            isRunning = false;
            animator.SetBool(running, false);
            animator.SetBool(jumping, true);
            DOVirtual.Float(speed, 0, 3.5f, SlowDownCallback).SetEase(Ease.OutQuint).OnComplete(()=> ActivateJoints());
            DOVirtual.Float(1, 0, 3.5f, SlowDownAnimation).SetEase(Ease.OutQuint);
            //ActivateJoints();
        }

        public void ThirdSequence()
        {
            Debug.Log("Third Sequence");
            DeactivateHitBox();
            DOVirtual.Float(0, speed, 2f, ContinueCallback).SetEase(Ease.InSine).OnComplete(()=> isJumpFinished = true);
        }

        private void ContinueCallback(float value)
        {
            distanceTravelled += value * Time.deltaTime;
            transform.parent.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
            
        }

        private void SlowDownAnimation(float value)
        {
            animator.speed = value;
        }

        private void SlowDownCallback(float value)
        {
            distanceTravelled += value * Time.deltaTime;
            transform.parent.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
            
        }

        public void ActivateJoints()
        {
            leftLegTBIK.data.target.position = leftFoot.position;
            rightLegTBIK.data.target.position = rightFoot.position;
            foreach (GameObject h in hitBoxs)
            {
                h.transform.DOScale(1, .15f).SetEase(Ease.OutSine);
            }
            DOVirtual.Float(0, 1, .5f, AnimateWeight);
        }

        private void AnimateWeight(float value)
        {
            leftLegTBIK.weight = value;
            rightLegTBIK.weight = value;
        }
        public void ActiveRun()
        {
            animator.speed = 1;
            animator.SetBool(running, true);
            animator.SetBool(jumping, false);
        }

        public void ActivateButton()
        {
            uiButton.DOScale(1, 1f).SetEase(Ease.OutBounce);
        }
        public void DeactivateHitBox()
        {         
            DOVirtual.DelayedCall(.5f, DelayedCall);

            uiButton.DOScale(0, 1f).SetEase(Ease.InBounce);
            foreach (GameObject h in hitBoxs)
            {
                h.SetActive(false);
            }
        }

        void DelayedCall()
        {
            DOVirtual.Float(1, 0, 2f, AnimateWeight).SetEase(Ease.InSine);
        }

        public void RagdollControl(bool activate)
        {
            isRagdoll = true;
            continueButton.gameObject.SetActive(false);
            DOVirtual.Float(1, 0, .1f, AnimateWeight);
            DOTween.KillAll();
            animator.enabled = !activate;
            foreach (Collider col in colliders)
            {
                col.enabled = activate;
                col.attachedRigidbody.isKinematic = !activate;
                col.attachedRigidbody.useGravity = activate;

            }
        }

        public void ApplyForce(float force)
        {
            Vector3 forceDirection = obstacle.transform.forward * force;
            forceDirection.y = 1;
            var rigidbody = colliders[0].gameObject.GetComponent<Rigidbody>();
            
            rigidbody.AddForce(forceDirection, ForceMode.VelocityChange);
        }
        public void Celebrate()
        {
            animator.SetBool(running, false);
            animator.SetBool(celebrate, true);
            isJumpFinished = false;
            if(!isRagdoll)
                vCam1.LookAt = null;

        }

        void OnPathChanged()
        {
            distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.parent.position);
        }

    }
}
