using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using FRIA;

namespace SaiyaJinn
{
    public abstract class BaseEffector: MonoBehaviour
    {
        public abstract GameObject EffectorSelection(out RaycastHit hit);
        public abstract void Move();

    }
}
