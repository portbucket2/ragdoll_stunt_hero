using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using UnityEngine.Animations.Rigging;
using System;
using PathCreation;

namespace SaiyaJinn
{
    [CreateAssetMenu(fileName = "New Middle Sequence", menuName = "ScriptableSequence/Middle Sequence", order = 51)]
    public class MiddleSequenceSO : ScriptableAction<PathCreator>
    {

        [SerializeField]
        private float moveDistance;
        [SerializeField]
        private float moveDuration;
        private float animationSpeed = 1;
        public UnityEvent transitionEvent;

        public bool isTaskDone;
        private Animator animController;
        public override bool PerformSequence(Animator animator, int previousState, int currentState, PathCreator path)
        {
            Transform animatedObject = animator.GetComponent<Transform>();
            Tween moveTween = animatedObject.DOMoveZ(animatedObject.position.z + moveDistance, moveDuration).SetEase(Ease.OutQuart);
            animController = animator;
            SlowMoCallback(animator, moveTween, previousState, currentState);
            return isTaskDone;
        }

        async void SlowMoCallback(Animator animator, Tween tween, int previous, int current)
        {
            await ShowMoAsync(animator, tween, previous, current);
            transitionEvent.Invoke();
            Debug.Log("Ready For Transition");
            //rightLegTBIK.weight = 1;
            isTaskDone = true;
        }
        


        async UniTask ShowMoAsync(Animator animator, Tween tween, int previousState, int currentState)
        {
            await tween.AsyncWaitForPosition(0.25f);
            animator.SetBool(previousState, false);
            animator.SetBool(currentState, true);
            //animator.CrossFadeInFixedTime(currentState, .25f);
            Tween speedUpdaterTween = DOVirtual.Float(1f, 0f, 4f, AnimatorUpdate).SetEase(Ease.OutQuart);
            await UniTask.WaitUntil(() => !speedUpdaterTween.IsActive());
            tween.Kill();
            await UniTask.Yield();

        }

        private void AnimatorUpdate(float speed)
        {
            animController.speed = speed;
     
            //Debug.Log(animController.speed);

        }


    }
}
