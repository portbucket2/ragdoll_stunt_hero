using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using UnityEngine.Animations.Rigging;
using DG.Tweening;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PathCreation;

namespace SaiyaJinn
{
    public class LevelOne : MonoBehaviour
    {
        [Title("Common Section")]
        public Button restartBtn;
        public PathCreator path;
        [Title("Tap Action")]
        private bool isTapped = false;
        int idle = Animator.StringToHash("idle");
        [Title("First Sequence Section")]
        [EnumToggleButtons]
        [GUIColor(0, .75f, .75f)]
        public ActionType actionTypeOne;
        int running = Animator.StringToHash("Run");
        public ScriptableAction<PathCreator> firstSequence;
        public UnityEvent OnFirstSequence;

        [Title("Second Sequence Section")]
        [SerializeField]
        private GameObject leftLegObject;
        [SerializeField]
        private GameObject rightLegObject;
        private TwoBoneIKConstraint leftLegTBIK;
        private TwoBoneIKConstraint rightLegTBIK;
        [SerializeField]
        private Transform leftFoot;
        [SerializeField]
        private Transform rightFoot;
        public GameObject[] hitBoxs = new GameObject[2];
        public RectTransform uiButton;
        //[EnumToggleButtons]
        //[GUIColor(0, .75f, 1f)]
        //public ActionType actionTypeTwo;
        public ScriptableAction<PathCreator> secondSequence;
        public UnityEvent OnSecondSequence;
        int jumping = Animator.StringToHash("Jump");

        [Title("Third Sequence Section")]
        public ScriptableAction<int> thirdSequence;
        int jumpDown = Animator.StringToHash("JumpDown");
        int celebrate = Animator.StringToHash("Celebrate");
        [SerializeField]
        private Button continueBtn;
        public List<Collider> colliders = new List<Collider>();
        public GameObject obstacle;
        [Title("Animation Section")]
        public Animator animator;
       
        //Hash
        

        private void Start()
        {
            OnFirstSequence.AddListener(FirstSequenceHandler);
            StartCoroutine(CheckTapRoutine());
            leftLegTBIK = leftLegObject.GetComponent<TwoBoneIKConstraint>();
            rightLegTBIK = rightLegObject.GetComponent<TwoBoneIKConstraint>();
            continueBtn.onClick.AddListener(DeactivateHitBox);
            restartBtn.onClick.AddListener(RestartScene);
        }
        private void Update()
        {
            if(Input.GetMouseButtonDown(0) && !isTapped)
            {
                isTapped = true;
            }
        }

        void RestartScene()
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
            DOTween.KillAll();
        }
        IEnumerator CheckTapRoutine()
        {
            yield return new WaitUntil(() => isTapped);
            if (OnFirstSequence != null)
                OnFirstSequence.Invoke();
        }

        public void FirstSequenceHandler()
        {
            firstSequence.PerformSequence(animator, idle, running, path);
            
            
        }
        public void FirstToSecondStateTransition()
        {
            secondSequence.PerformSequence(animator, running, jumping, path);
        }


        public void ActivateJoints()
        {
            leftLegTBIK.data.target.position = leftFoot.position;
            rightLegTBIK.data.target.position = rightFoot.position;
            foreach(GameObject h in hitBoxs)
            {
                h.transform.DOScale(1, .15f).SetEase(Ease.OutSine);
            }
            DOVirtual.Float(0, 1, .5f, AnimateWeight);
        }

        private void AnimateWeight(float value)
        {
            leftLegTBIK.weight = value;
            rightLegTBIK.weight = value;
        }
        public void ActivateButton()
        {
            uiButton.DOScale(1, 1f).SetEase(Ease.OutBounce);
        }
        public void DeactivateHitBox()
        {
            thirdSequence.PerformSequence(animator, jumping, jumpDown, celebrate);
            DOVirtual.DelayedCall(.5f, DelayedCall);
            
            uiButton.DOScale(0, 1f).SetEase(Ease.InBounce);
            foreach (GameObject h in hitBoxs)
            {
                h.SetActive(false);
            }
        }

        void DelayedCall()
        {
            DOVirtual.Float(1, 0, 1.5f, AnimateWeight);
        }
        public void RagdollControl(bool activate)
        {
            continueBtn.gameObject.SetActive(false);
            DOVirtual.Float(1, 0, .1f, AnimateWeight);
            DOTween.KillAll();
            animator.enabled = !activate;
            foreach (Collider col in colliders)
            {
                col.enabled = activate;
                col.attachedRigidbody.isKinematic = !activate;
                col.attachedRigidbody.useGravity = activate;

            }
        }

        public void ApplyForce(float force)
        {
            Vector3 forceDirection = obstacle.transform.forward * force;
            forceDirection.y = 1;
            var rigidbody = animator.GetBoneTransform(HumanBodyBones.Hips).GetComponent<Rigidbody>();
            rigidbody.AddForce(forceDirection, ForceMode.VelocityChange);
        }
    }
}
