using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using System;
using PathCreation;


namespace SaiyaJinn
{
    [CreateAssetMenu(fileName ="New Initial Sequence", menuName = "ScriptableSequence/Initial Sequence", order = 51)]
    public class InitialSequenceSO : ScriptableAction<PathCreator>
    {
        [SerializeField]
        private float moveDistance;
        [SerializeField]
        private float moveDuration;
        private float animationSpeed = 1;
        public UnityEvent transitionEvent;
       
        public bool isTaskDone;
        private Animator animController;

        public float GetMoveDistance
        {
            get
            {
                return moveDistance;
            }
        }

        public float GetMoveDuration
        { 
            get
            {
                return moveDuration;
            }
        }

        public override bool PerformSequence(Animator animator, int previousState, int currentState, PathCreator path)
        {
            //animator.SetBool(previousState, false);
            //animator.SetBool(currentState, true);
            Transform animatedObject = animator.GetComponent<Transform>();
            //Tween moveTween = animatedObject.DOMoveZ(moveDistance, moveDuration).SetEase(Ease.Linear);
            // Callback
            animController = animator;
            //SlowMoCallback(animator);
            
            return isTaskDone;
            

        }
        void Update()
        {
            Debug.Log("Update");
        }
        /*
        async void SlowMoCallback(Animator animator)
        {
            await NoSlowMoAsynce(animator);
            transitionEvent.Invoke();
            isTaskDone = true;
        }

        async UniTask NoSlowMoAsynce(Animator animator)
        {
            //await tween.AsyncWaitForPosition(0.75f);
            //Tween speedUpdaterTween = DOVirtual.Float(1f, 0f, 2f, AnimatorUpdate).SetEase(Ease.OutQuart);
            //await UniTask.WaitUntil(() => !speedUpdaterTween.IsActive());
            //await tween.AsyncWaitForCompletion();
            await UniTask.Yield();
            
        }
        private void AnimatorUpdate(float speed)
        {
            animController.speed = speed;
            
        }
        
        */
    }

    public enum ActionType
    {
        INITIAL,
        SLOWMO,
        NONSLOWMO,
        RAGDOLL,
        SUCCESS
    }

   

}
