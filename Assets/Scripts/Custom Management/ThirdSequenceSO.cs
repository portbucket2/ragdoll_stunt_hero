using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System;
using Cysharp.Threading.Tasks;

namespace SaiyaJinn
{
    [CreateAssetMenu(fileName = "New Third Sequence", menuName = "ScriptableSequence/Third Sequence", order = 51)]
    public class ThirdSequenceSO : ScriptableAction<int>
    {
        [SerializeField]
        private float moveDistance;
        [SerializeField]
        private float moveDuration;
        private float animationSpeed = 1;
        public UnityEvent completionEvent;
        public Ease ease;

        public bool isTaskDone;
        private Animator animController;
        public override bool PerformSequence(Animator animator, int previousState, int currentState, int genericParam)
        {
            Transform animatedObject = animator.GetComponent<Transform>();
            Tween moveTween = animatedObject.DOMoveZ(animatedObject.position.z + moveDistance, moveDuration).SetEase(ease);
            animController = animator;
            ClimbDownCallback(animator,moveTween, previousState, currentState, genericParam);
            return isTaskDone;
        }

        async void ClimbDownCallback(Animator animator, Tween tween, int prevState, int currState, int celebrateState)
        {
            await ClimbDownAsync(animator, tween, prevState, currState, celebrateState);
            completionEvent.Invoke();
            isTaskDone = true;
        }

        async UniTask ClimbDownAsync(Animator animator, Tween tween, int previousState, int currentState, int celebrateState)
        {
            await tween.AsyncWaitForPosition(0.25f);
            animator.SetBool(previousState, false);
            animator.SetBool(currentState, true);
            Tween speedUpdaterTween = DOVirtual.Float(0f, 1f, 2f, AnimatorUpdate).SetEase(Ease.OutQuart);
            await tween.AsyncWaitForCompletion();
            animator.SetBool(currentState, false);
            animator.SetBool(celebrateState, true);
        }

        private void AnimatorUpdate(float value)
        {
            animController.speed = value;

        }
    }
}
