using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace SaiyaJinn
{
    public abstract class ScriptableAction<T> : ScriptableObject
    {
        public abstract bool PerformSequence(Animator animator, int previousState, int currentState, T genericParam);
        //public abstract bool isTaskComplete { get; set; }
    }
}
