﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class InstantMeshMorf : MonoBehaviour
{
    public Mesh mesh0;
    public Mesh mesh1;
    [Range(0,1)]
    public float progress;
    
    public void MorfNow()
    {

        int N1 = mesh0.vertexCount;
        int N2 = mesh1.vertexCount;
        if (N1 != N2)
        {
            Debug.LogError("mesh vertex count dont match");
            return;
        }
        MeshFilter mf = GetComponent<MeshFilter>();
        if (mf.mesh != null)
        {
            Destroy(mf.mesh);
            mf.mesh = null;
        }
        Mesh m = Instantiate(mesh0);
        mf.mesh = m;


        Vector3[] vertices = new Vector3[N1];
        Vector3[] normals = new Vector3[N1];
        Vector3[] v0 = mesh0.vertices;
        Vector3[] v1 = mesh1.vertices;
        Vector3[] n0 = mesh0.normals;
        Vector3[] n1 = mesh1.normals;

        for (int i = 0; i < N1; i++)
        {
            vertices[i] = Vector3.Lerp(v0[i],v1[i],progress);
            normals[i] = Vector3.Lerp(n0[i], n1[i], progress);
        }
        m.SetVertices(vertices);
        m.SetNormals(normals);

        
    }

}
#if UNITY_EDITOR
[CustomEditor(typeof(InstantMeshMorf))]
public class InstantMeshMorfEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("MorfNow"))
        {
            (target as InstantMeshMorf).MorfNow();
        }
    }
}
#endif