﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class OverlapDetection : MonoBehaviour
{

    private void Awake()
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
    }

    public bool IsOverLapping { get { return items.Count > 0; } }
    public List<Collider> items = new List<Collider>();


    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log("in");
        items.Add(other);
    }
    public void OnTriggerExit(Collider other)
    {
        //Debug.Log("in");
        items.Remove(other);
    }
}
