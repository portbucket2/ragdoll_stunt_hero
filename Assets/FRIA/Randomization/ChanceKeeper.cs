﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FRIA
{
    [System.Serializable]
    public class ChanceKeeper : ScriptableObject
    {
        public int pseudoSize = 10;
        public float pseudoStrength = 10;
        public List<ElementChance> options = new List<ElementChance>();

        [System.NonSerialized] public PseudoChancedList<GameObject> clist;
        public void Init()
        {
            clist = new PseudoChancedList<GameObject>();
            clist.Refresh(pseudoSize, pseudoStrength);
            foreach (var item in options)
            {
                clist.Add(item.element, item.chance);
            }
            clist.Normalize();
        }

        public GameObject Roll()
        {
            if (clist == null)
            {
                Debug.LogError("Chance Manager not initialized...");
                return null;
            }
            return clist.Roll();
        }

    }
    [System.Serializable]
    public class ElementChance
    {
        public GameObject element;
        public float chance = 1;
    }
}