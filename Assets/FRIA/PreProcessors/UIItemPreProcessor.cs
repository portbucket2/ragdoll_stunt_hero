﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

public class UIItemPreProcessor : AssetPostprocessor
{
    void OnPreprocessAsset()
    {
        if (assetPath.Contains("Assets/2D/UI"))  // @-sign in the name triggers this step
        {
            //ModelImporter modelImporter = assetImporter as ModelImporter;
            //if (modelImporter != null)
            //{
            //    Debug.LogFormat("PreProcessing {0}", modelImporter.name);
            //    modelImporter.optimizeMeshVertices = false;
            //}

            TextureImporter textureImporter = assetImporter as TextureImporter;
            if (textureImporter)
            {
                Debug.LogFormat("PreProcessing {0}", textureImporter.name);
                textureImporter.textureType = TextureImporterType.Sprite; 
                textureImporter.mipmapEnabled = true;
                textureImporter.crunchedCompression = true;
                textureImporter.compressionQuality = 100;
                if(textureImporter.maxTextureSize>1024) textureImporter.maxTextureSize = 1024;
                
            }
        }
    }
}

#endif

