﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager Instance { get; private set; }
    public static IStateMateAccess<GameState> stateAccess { get { return Instance.stateMate; } }
    public static GameState State { get { return stateAccess.CurrentState; } }
    public static bool inAction { get { return State == GameState.Action; } }
    
    private StateMate<GameState> stateMate;

    public List<GameObject> startObjs;
    public List<GameObject> purchaseObjs;
    public List<GameObject> inGameObjs;
    private void Awake()
    {
        Instance = this;
        stateMate = new StateMate<GameState>(initial: GameState.Init, enableDefinedTransitionOnly: true, loadEmpties: true);
        stateMate.AddStateWithEnableList(GameState.Init,startObjs);
        stateMate.AddStateWithEnableList(GameState.Purchase, purchaseObjs);
        stateMate.AddStateWithEnableList(GameState.Action, inGameObjs);
        stateMate.AddTransition(GameState.Init, GameState.Purchase);
        stateMate.AddTransition(GameState.Purchase, GameState.Init);
        stateMate.AddTransition(GameState.Init, GameState.Action);
        stateMate.AddTransition(GameState.Action, GameState.Fail);
        stateMate.AddTransition(GameState.Fail, GameState.Action);
        stateMate.AddTransition(GameState.Action, GameState.Success);
        //stateMate.logStateSwitches = true;

    }
    private void Start()
    {
        stateMate.SetInitialSwitches();
    }
    public static void RequestSwitch(GameState gs)
    {
        Instance.stateMate.SwitchState(gs);
    }
}

public enum GameState
{
    Init = 0,
    Purchase = 1,
    Action = 2,
    Fail = 3,
    Success = 4, 
}